import 'dart:developer';

import 'package:digi_edge_test/core/repositories/core_repository.dart';
import 'package:digi_edge_test/features/Auth/domain/entities/country_code.dart';
import 'package:get/get.dart';

import '../../features/Auth/presentation/widgets/country_codes_dialog.dart';

class CountryCodesController {
  CoreRepository repository = Get.find();
  List<CountryCode> countryCodes = <CountryCode>[];

  void showCountryCodes(CountryCode slectedCode, Function onChenge) async {
    if (countryCodes.isEmpty) {
      await loadCountryCodes();
    }
    Get.dialog(CountryCodesDialog(
      selectedValue: slectedCode,
      onChange: onChenge,
      countryCodes: countryCodes,
    ));
  }

  Future<void> loadCountryCodes() async {
    var action = await repository.loadCountryCodes();
    action.fold((l) {
      log(l.message, name: 'Failure');
    }, (r) {
      log(r.length.toString(), name: 'COUNTRY_CODES');
      countryCodes = r;
    });
  }
}
