import 'package:dartz/dartz.dart';
import 'package:digi_edge_test/core/services/json_loader.dart';
import 'package:digi_edge_test/core/utils/failure.dart';
import 'package:digi_edge_test/features/Auth/domain/entities/country_code.dart';
import 'package:get/get.dart';

class CoreRepository {
  JsonLoader jsonLoader = Get.find();

  Future<Either<Failure, List<CountryCode>>> loadCountryCodes() async {
    try {
      return Right(await jsonLoader.loadCountryCodes());
    } catch (e) {
      return Left(Failure(message: e.toString()));
    }
  }
}
