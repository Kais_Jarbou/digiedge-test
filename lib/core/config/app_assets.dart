abstract class AppAssets {
  static const String images = 'assets/images';
  static const String logo = '$images/ali_fouad_logo.png';
}
