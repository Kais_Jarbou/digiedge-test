import 'package:flutter/material.dart';

abstract class AppStyle {
  static const Color purple = Color(0xFF432839);
  static const Color white = Colors.white;
  static Color red = Colors.red[800] ?? Colors.red;
  static Color green = Colors.green;

  static InputDecoration inputDecoration = InputDecoration(
      contentPadding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      errorBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
      border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)));
}
