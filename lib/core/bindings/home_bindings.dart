import 'package:digi_edge_test/features/Home/data/data_sources/user_info_api.dart';
import 'package:digi_edge_test/features/Home/domain/repositories/home_repository.dart';
import 'package:digi_edge_test/features/Home/presentation/controllers/home_controller.dart';
import 'package:get/get.dart';

class HomeBindings extends Bindings {
  @override
  void dependencies() {
    Get.put(UserInfoApi());
    Get.put(HomeRepository());
    Get.put(HomeController());
  }
}
