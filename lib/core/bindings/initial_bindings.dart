import 'package:digi_edge_test/core/controllers/country_codes_controller.dart';
import 'package:digi_edge_test/core/repositories/core_repository.dart';
import 'package:digi_edge_test/core/services/http_service.dart';
import 'package:digi_edge_test/core/services/json_loader.dart';
import 'package:digi_edge_test/core/services/local_storage_service.dart';
import 'package:get/get.dart';
import '../../features/Auth/presentation/views/welcome_page.dart';

class InitialBindings extends Bindings {
  @override
  void dependencies() {
    Get.putAsync<LocalStorageService>(() => LocalStorageService().init(),
        permanent: true);
    Get.put<HttpService>(HttpService(), permanent: true);
    Get.put<InitialController>(
      InitialController(),
    );
    Get.put<JsonLoader>(
      JsonLoader(),
    );
    Get.put<CoreRepository>(
      CoreRepository(),
    );
    Get.put<CountryCodesController>(
      CountryCodesController(),
    );
  }
}

class InitialController extends GetxController {
  @override
  void onReady() {
    super.onReady();
    Get.offAllNamed(WelcomePage.routeName);
  }
}
