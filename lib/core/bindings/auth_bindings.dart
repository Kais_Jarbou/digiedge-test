import 'package:digi_edge_test/features/Auth/data/data_sources/auth_api.dart';
import 'package:digi_edge_test/core/services/json_loader.dart';
import 'package:digi_edge_test/features/Auth/domain/repository/auth_repository.dart';
import 'package:get/get.dart';

class AuthBindings extends Bindings {
  @override
  void dependencies() {
    Get.put<AuthApi>(AuthApi());
    Get.put<JsonLoader>(JsonLoader());
    Get.put<AuthRepository>(AuthRepository());
  }
}
