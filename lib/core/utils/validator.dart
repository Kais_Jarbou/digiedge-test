abstract class Validator {
  static String? isNotEmpty(String? value) {
    if (value == null || value.isEmpty) {
      return 'must not be empty';
    }
    return null;
  }
}
