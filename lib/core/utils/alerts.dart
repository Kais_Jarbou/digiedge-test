import 'package:digi_edge_test/core/config/app_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

abstract class Alerts {
  static void error(String title, String message) {
    Get.snackbar(title, message,
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: AppStyle.red,
        messageText: Text(
          message,
          style: const TextStyle(
              fontWeight: FontWeight.bold, color: AppStyle.white),
        ),
        colorText: AppStyle.white);
  }

  static void success(String title, String message) {
    Get.snackbar(title, message,
        snackPosition: SnackPosition.BOTTOM,
        messageText: Text(
          message,
          style: const TextStyle(
              fontWeight: FontWeight.bold, color: AppStyle.white),
        ),
        backgroundColor: AppStyle.green,
        colorText: AppStyle.white);
  }
}
