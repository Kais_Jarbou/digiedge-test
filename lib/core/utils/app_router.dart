import 'package:digi_edge_test/core/bindings/auth_bindings.dart';
import 'package:digi_edge_test/core/bindings/home_bindings.dart';
import 'package:digi_edge_test/features/Auth/presentation/controllers/login_controller.dart';
import 'package:digi_edge_test/features/Auth/presentation/controllers/welcome_controller.dart';
import 'package:digi_edge_test/features/Auth/presentation/views/login_page.dart';
import 'package:digi_edge_test/features/Auth/presentation/views/register_page.dart';
import 'package:digi_edge_test/features/Auth/presentation/views/welcome_page.dart';
import 'package:digi_edge_test/features/Home/presentation/views/change_password_page.dart';
import 'package:digi_edge_test/features/Home/presentation/views/home_page.dart';
import 'package:get/get.dart';

import '../../features/Auth/presentation/controllers/register_controller.dart';
import '../../features/Home/presentation/views/user_info_page.dart';

abstract class AppRouter {
  static List<GetPage> routes = [
    GetPage(
        name: WelcomePage.routeName,
        page: () => const WelcomePage(),
        binding: BindingsBuilder(() {
          Get.put(WelcomeController());
        })),
    GetPage(
        name: LoginPage.routeName,
        page: () => const LoginPage(),
        binding: BindingsBuilder(() {
          AuthBindings().dependencies();
          Get.put<LoginController>(LoginController());
        })),
    GetPage(
        name: RegisterPage.routeName,
        page: () => const RegisterPage(),
        binding: BindingsBuilder(() {
          AuthBindings().dependencies();
          Get.put<RegisterController>(RegisterController());
        })),
    GetPage(
        name: HomePage.routeName,
        page: () => HomePage(),
        binding: HomeBindings()),
    GetPage(
        name: UserInfoPage.routeName,
        page: () => const UserInfoPage(),
        binding: HomeBindings()),
    GetPage(
        name: ChangePasswordPage.routeName,
        page: () => const ChangePasswordPage(),
        binding: HomeBindings()),
  ];
}
