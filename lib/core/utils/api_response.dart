import 'dart:convert';
import 'package:http/http.dart' as http;

class ApiResponse {
  final int statusCode;
  final bool success;
  final String message;
  final dynamic data;

  ApiResponse({
    required this.statusCode,
    required this.success,
    required this.message,
    required this.data,
  });

  factory ApiResponse.fromJson(http.Response response) {
    var jsonResponse = jsonDecode(response.body);
    return ApiResponse(
        statusCode: response.statusCode,
        success: jsonResponse['success'],
        message: jsonResponse['message'],
        data: jsonResponse['data']);
  }
}
