import 'dart:convert';
import 'dart:developer';

import 'package:digi_edge_test/core/services/local_storage_service.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;

class HttpService extends http.BaseClient {
  static const String baseUrl = 'http://testapi.alifouad91.com/api';
  static const List<String> routesWithNoAuthorization = [
    '/api/user/register',
    '/api/login'
  ];

  @override
  Future<http.Response> get(
    Uri url, {
    Map<String, String>? headers,
    Encoding? encoding,
  }) async {
    headers = authorizeRequest(url, headers);
    logRequest(url);

    http.Response response = await super.get(
      url,
      headers: headers,
    );
    logResponse(response);
    return response;
  }

  @override
  Future<http.Response> delete(
    Uri url, {
    Map<String, String>? headers,
    Object? body,
    Encoding? encoding,
  }) async {
    headers = authorizeRequest(url, headers);
    logRequest(url, body: body.toString());
    http.Response response =
        await super.delete(url, headers: headers, body: body);
    logResponse(response);
    return response;
  }

  @override
  Future<http.Response> post(Uri url,
      {Map<String, String>? headers, Object? body, Encoding? encoding}) async {
    headers = authorizeRequest(url, headers);
    logRequest(url, body: body.toString());
    http.Response response =
        await super.post(url, headers: headers, body: body);
    logResponse(response);
    return response;
  }

  Map<String, String> authorizeRequest(Uri url, Map<String, String>? headers) {
    // ignore: prefer_conditional_assignment
    if (headers == null) {
      // ignore: prefer_collection_literals
      headers = Map<String, String>();
    }
    if (routesWithNoAuthorization.contains(url.path.toString())) return headers;

    final LocalStorageService localStorageService = Get.find();
    String authToken = localStorageService.authToken;
    headers.putIfAbsent("Authorization", () => 'Bearer $authToken');
    return headers;
  }

  void logRequest(url, {String body = ""}) {
    log(url.path, name: 'REQUEST');
    if (body.isNotEmpty) {
      log(body.toString(), name: 'BODY');
    }
  }

  void logResponse(response) {
    log((response as http.Response).body.toString(),
        name: 'RESPONSE (${response.statusCode})');
  }

  @override
  Future<http.StreamedResponse> send(http.BaseRequest request) {
    log('----------------REQUEST----------------');
    request.headers.putIfAbsent('Accept', () => 'application/json');
    request.headers.putIfAbsent('Content-Type', () => 'application/json');
    return request.send();
  }
}

// "OK29Gff3QCMV6xGkc=wdaBvZ@JHQ=Pk%pgGGml!$VwGrtEIsP0wIpEd&_FBlTUL#0LXfD&%vwm4YS5Zys_gBkt6mHIKQvmXh5sAVSYLwfLtATm5ouuUxc0#qXZ&w2YtB^bujiQ=dfA0x32$!KRf63MH6vmaWh$ha+9^HGD1KAM6sLVmR#ist8qZKxS9v1$O@Vxr#J%k_lK7!FIVe5Rq_d3FVG2#%Qp@=qQsGN#3=hh4Ud@J@o2CBSj4W*^+2ZnNw#MV0=NU338mPessnfH0!l7#Be_M=AU#mbm*_U%$_SrMrbee&5f+H$0lhSNfQMfcS0@5MLP=SdOba4JoPtJYavOFRoA_Ca1$*5pXPfzOsQX=8hozkJHkA17v$Knr6A0&Myy7^x5lNkQdy@H%+qD50j*CHGkhN8OD7_O^hO&YkU1pRiLE0vl7ZX&TMiPjYNmI%Uj4jye%$%T+v7P470OzxzWAHS85$dbb@Eque3RVqtWqHH5yAHsh$J^IicQjsfvB5LcSGeX1MTL&5lS2d2Lwv9T^YRkgfHL1Lc8oeQdhxm5fYyyMvQt9nP0hNp=xfNJmRku58NZzR0+A8ir*3OyZlsk3DWU8pHe6$v6GIz_y7BMu=GPk5Mu6HxLKO81nqgu7Z6@096XQpoHUDlkHZ6IftPO#_=pM$Skt=6D@v0fFQz+iOA4s%+oQP5pL+ohulxPh$I^j@xks^dS1T_3VTeMkjimJbUcaNAb*fBnb&zP4wSx=yOGY+zVth2JR4gB9o__FHbBK!=QcqDW!gwNnpsWnDIhvGOOcKI4JluPU5hNDW1lOvjw%sj!Y0A#1vNcNwZ^#&KC3=R#&VbL33tP_pFBI+K&V#lhNFKLlrLoMozendUkUk9QvhFUqkG6e1Z91fS*=ZUQ*$mfP@Iqxjj84zuIf3mwSU89egDypFmrAdkg^gf5^N_t^4EOlqq2M96d#zk%8=Zkve0*6zqUF3r#qwV%qD&hD@xJiq%k!Refq!&@b!"
// "OK29Gff3QCMV6xGkc=wdaBvZ@JHQ=Pk%pgGGml!$VwGrtEIsP0wIpEd&_FBlTUL#0LXfD&%vwm4YS5Zys_gBkt6mHIKQvmXh5sAVSYLwfLtATm5ouuUxc0#qXZ&w2YtB^bujiQ=dfA0x32$!KRf63MH6vmaWh$ha+9^HGD1KAM6sLVmR#ist8qZKxS9v1$O@Vxr#J%k_lK7!FIVe5Rq_d3FVG2#%Qp@=qQsGN#3=hh4Ud@J@o2CBSj4W*^+2ZnNw#MV0=NU338mPessnfH0!l7#Be_M=AU#mbm*_U%$_SrMrbee&5f+H$0lhSNfQMfcS0@5MLP=SdOba4JoPtJYavOFRoA_Ca1$*5pXPfzOsQX=8hozkJHkA17v$Knr6A0&Myy7^x5lNkQdy@H%+qD50j*CHGkhN8OD7_O^hO&YkU1pRiLE0vl7ZX&TMiPjYNmI%Uj4jye%$%T+v7P470OzxzWAHS85$dbb@Eque3RVqtWqHH5yAHsh$J^IicQjsfvB5LcSGeX1MTL&5lS2d2Lwv9T^YRkgfHL1Lc8oeQdhxm5fYyyMvQt9nP0hNp=xfNJmRku58NZzR0+A8ir*3OyZlsk3DWU8pHe6$v6GIz_y7BMu=GPk5Mu6HxLKO81nqgu7Z6@096XQpoHUDlkHZ6IftPO#_=pM$Skt=6D@v0fFQz+iOA4s%+oQP5pL+ohulxPh$I^j@xks^dS1T_3VTeMkjimJbUcaNAb*fBnb&zP4wSx=yOGY+zVth2JR4gB9o__FHbBK!=QcqDW!gwNnpsWnDIhvGOOcKI4JluPU5hNDW1lOvjw%sj!Y0A#1vNcNwZ^#&KC3=R#&VbL33tP_pFBI+K&V#lhNFKLlrLoMozendUkUk9QvhFUqkG6e1Z91fS*=ZUQ*$mfP@Iqxjj84zuIf3mwSU89egDypFmrAdkg^gf5^N_t^4EOlqq2M96d#zk%8=Zkve0*6zqUF3r#qwV%qD&hD@xJiq%k!Refq!&@b!""