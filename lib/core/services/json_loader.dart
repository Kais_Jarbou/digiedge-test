import 'dart:convert';

import 'package:digi_edge_test/features/Auth/domain/entities/country_code.dart';
import 'package:flutter/services.dart';

class JsonLoader {
  Future<List<CountryCode>> loadCountryCodes() async {
    List jsonList = await rootBundle
        .loadString('assets/json/country_codes.json')
        .then((jsonStr) => jsonDecode(jsonStr));
    return jsonList.map((e) => CountryCode.fromJson(e)).toList();
  }
}
