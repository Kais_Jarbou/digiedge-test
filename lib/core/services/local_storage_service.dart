import 'package:digi_edge_test/features/Auth/data/models/user_model.dart';
import 'package:digi_edge_test/features/Auth/domain/entities/user.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

class LocalStorageService extends GetxService {
  late final GetStorage storage;

  Future<LocalStorageService> init() async {
    GetStorage.init();
    storage = GetStorage();
    return this;
  }

  bool get isLoggedIn {
    if (storage.read(_authToken) != null && !isAuthTokenExpired) {
      return true;
    }
    return false;
  }

  bool get isAuthTokenExpired {
    String authTokenExpiry = storage.read(_authTokenExpiry);
    DateTime expiryDate = DateTime.parse(authTokenExpiry);
    if (expiryDate.isBefore(DateTime.now())) return true;
    return false;
  }

  Future<void> setAuthTokenExpiry(String value) async {
    await storage.write(_authTokenExpiry, value);
    return;
  }

  String get authToken {
    return storage.read(_authToken);
  }

  Future<void> setAuthToken(String value) async {
    await storage.write(_authToken, value);
    return;
  }

  UserModel get user {
    var json = storage.read((_user));
    return UserModel.fromJson(json);
  }

  Future<void> setUser(User value) async {
    await storage.write(_user, UserModel.fromEntity(value).toJson());
    return;
  }

  Future<void> clearData() async {
    await storage.erase();
  }

  // KEYS
  final String _authToken = "AUTH_TOKEN";
  final String _authTokenExpiry = "AUTH_TOKEN_EXPIRY";
  final String _user = "USER";
}
