import 'package:digi_edge_test/core/config/app_style.dart';
import 'package:flutter/material.dart';

class LoadingDialog extends StatelessWidget {
  const LoadingDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      backgroundColor: AppStyle.white,
      child: const Padding(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 32),
        child: PopScope(
          canPop: false,
          child: Text(
            'Loading ..',
            textAlign: TextAlign.center,
            style: TextStyle(color: AppStyle.purple),
          ),
        ),
      ),
    );
  }
}
