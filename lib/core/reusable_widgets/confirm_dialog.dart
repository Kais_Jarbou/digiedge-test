import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../config/app_style.dart';

class ConfirmDialog extends StatelessWidget {
  final String title;
  final String message;
  final Function onConfirm;
  const ConfirmDialog(
      {super.key,
      required this.title,
      required this.message,
      required this.onConfirm});

  @override
  Widget build(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        backgroundColor: AppStyle.white,
        child: Container(
            padding: const EdgeInsets.all(24),
            height: 240,
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              Text(
                title,
                style:
                    const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
              ),
              const SizedBox(
                height: 16,
              ),
              Text(
                message,
                style: const TextStyle(fontSize: 14),
              ),
              const SizedBox(
                height: 16,
              ),
              Row(
                children: [
                  Expanded(
                      child: Center(
                    child: TextButton(
                      onPressed: () => Get.back(),
                      child: Text(
                        'cancel',
                        style: TextStyle(
                            fontSize: 16,
                            color: AppStyle.red,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )),
                  const SizedBox(
                    width: 8,
                  ),
                  Expanded(
                      child: Center(
                    child: TextButton(
                      onPressed: () => onConfirm(),
                      child: Text(
                        'confirm',
                        style: TextStyle(
                            fontSize: 16,
                            color: AppStyle.green,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )),
                ],
              )
            ])));
  }
}
