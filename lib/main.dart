import 'package:digi_edge_test/core/bindings/initial_bindings.dart';
import 'package:digi_edge_test/core/config/app_style.dart';
import 'package:digi_edge_test/core/utils/app_router.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(GetMaterialApp(
    enableLog: false,
    home: const InitialApp(),
    initialBinding: InitialBindings(),
    defaultTransition: Transition.noTransition,
    getPages: AppRouter.routes,
    theme: ThemeData(
        fontFamily: 'AlexandriaFLF',
        scaffoldBackgroundColor: AppStyle.white,
        appBarTheme: const AppBarTheme(color: AppStyle.white)),
  ));
}

class InitialApp extends StatelessWidget {
  const InitialApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold();
  }
}
