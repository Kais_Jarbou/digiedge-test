import 'package:digi_edge_test/features/Auth/domain/entities/user.dart';

class UserModel extends User {
  UserModel(
      {required super.fullName,
      required super.id,
      required super.countryCode,
      required super.email,
      required super.phoneNumber});

  Map toJson() {
    return {
      'id': id,
      'name': fullName,
      'email': email,
      'country_code': countryCode,
      'phone': phoneNumber,
    };
  }

  factory UserModel.fromJson(Map json) => UserModel(
        id: json['id'],
        fullName: json['name'],
        email: json['email'],
        countryCode: json['country_code'],
        phoneNumber: json['phone'],
      );

  factory UserModel.fromEntity(User user) => UserModel(
      fullName: user.fullName,
      id: user.id,
      countryCode: user.countryCode,
      email: user.email,
      phoneNumber: user.phoneNumber);
}
