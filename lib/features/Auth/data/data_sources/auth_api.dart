import 'package:digi_edge_test/core/services/http_service.dart';
import 'package:digi_edge_test/core/utils/api_response.dart';
import 'package:get/instance_manager.dart';
import 'package:http/http.dart' as http;

class AuthApi {
  final String baseUrl = HttpService.baseUrl;
  final HttpService httpService = Get.find();
  Future<ApiResponse> register(
      {required String name,
      required String countryCode,
      required String phone,
      required String email,
      required String password,
      required String confirmPassword}) async {
    http.Response response =
        await httpService.post(Uri.parse('$baseUrl/user/register'), body: {
      'name': name,
      'email': email,
      'country_code': countryCode,
      'phone': phone,
      'password': password,
      'password_confirm': password
    });
    ApiResponse apiResponse = ApiResponse.fromJson(response);
    return apiResponse;
  }

  Future<ApiResponse> login(
      {required String email, required String password}) async {
    http.Response response =
        await httpService.post(Uri.parse('$baseUrl/login'), body: {
      'email': email,
      'password': password,
    });
    ApiResponse apiResponse = ApiResponse.fromJson(response);
    return apiResponse;
  }
}
