import 'package:dartz/dartz.dart';
import 'package:digi_edge_test/core/services/local_storage_service.dart';
import 'package:digi_edge_test/core/utils/api_response.dart';
import 'package:digi_edge_test/core/utils/failure.dart';
import 'package:digi_edge_test/features/Auth/data/data_sources/auth_api.dart';
import 'package:digi_edge_test/features/Auth/data/models/user_model.dart';
import 'package:get/get.dart';

class AuthRepository {
  AuthApi api = Get.find();
  LocalStorageService localStorageService = Get.find();

  Future<Either<Failure, void>> register(
      {required String name,
      required String countryCode,
      required String phone,
      required String email,
      required String password,
      required String confirmPassword}) async {
    try {
      ApiResponse response = await api.register(
          name: name,
          countryCode: countryCode,
          phone: phone,
          email: email,
          password: password,
          confirmPassword: confirmPassword);
      if (response.success) {
        String authToken = response.data['token'];
        localStorageService.setAuthToken(authToken);
        String tokenExpiry = response.data['token_expiry'];
        localStorageService.setAuthTokenExpiry(tokenExpiry);
        UserModel userModel = UserModel.fromJson(response.data);
        localStorageService.setUser(userModel);
        return const Right(null);
      } else {
        return Left(Failure(message: response.message));
      }
    } catch (e) {
      return Left(Failure(message: e.toString()));
    }
  }

  Future<Either<Failure, void>> login({
    required String email,
    required String password,
  }) async {
    try {
      ApiResponse response = await api.login(email: email, password: password);
      if (response.success) {
        String authToken = response.data['token'];
        localStorageService.setAuthToken(authToken);
        String tokenExpiry = response.data['token_expiry'];
        localStorageService.setAuthTokenExpiry(tokenExpiry);
        UserModel userModel = UserModel.fromJson(response.data);
        localStorageService.setUser(userModel);
        return const Right(null);
      } else {
        return Left(Failure(message: response.message));
      }
    } catch (e) {
      return Left(Failure(message: e.toString()));
    }
  }
}
