import 'package:digi_edge_test/features/Auth/data/models/user_model.dart';

class User {
  final String id;
  final String fullName;
  final String countryCode;
  final String phoneNumber;
  final String email;

  User({
    required this.id,
    required this.fullName,
    required this.countryCode,
    required this.phoneNumber,
    required this.email,
  });

  factory User.fromModel(UserModel userModel) => User(
      id: userModel.id,
      fullName: userModel.fullName,
      countryCode: userModel.countryCode,
      phoneNumber: userModel.phoneNumber,
      email: userModel.email);
}
