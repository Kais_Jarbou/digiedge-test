class CountryCode {
  final String country;
  final String dialCode;
  final String flag;

  CountryCode(
      {required this.country, required this.dialCode, required this.flag});

  factory CountryCode.fromJson(json) => CountryCode(
      country: json['name'], dialCode: json['dial_code'], flag: json['flag']);
}
