import 'package:digi_edge_test/core/reusable_widgets/loading_dialog.dart';
import 'package:digi_edge_test/core/utils/alerts.dart';
import 'package:digi_edge_test/features/Auth/domain/repository/auth_repository.dart';
import 'package:digi_edge_test/features/Home/presentation/views/home_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class LoginController extends GetxController {
  final AuthRepository repository = Get.find();
  final GlobalKey<FormState> loginKey = GlobalKey<FormState>();

  final TextEditingController emailTEC = TextEditingController();
  final TextEditingController passwordTEC = TextEditingController();
  final obsecurePassword = true.obs;

  void login() async {
    if (loginKey.currentState!.validate()) {
      Get.dialog(const LoadingDialog(), barrierDismissible: false);
      var action = await repository.login(
          email: emailTEC.text, password: passwordTEC.text);
      Get.back();
      action.fold((l) {
        Alerts.error("Error", l.message);
      }, (r) {
        Get.offNamed(HomePage.routeName);
      });
    }
  }
}
