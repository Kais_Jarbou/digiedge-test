import 'package:digi_edge_test/core/reusable_widgets/loading_dialog.dart';
import 'package:digi_edge_test/core/utils/alerts.dart';
import 'package:digi_edge_test/features/Auth/domain/entities/country_code.dart';
import 'package:digi_edge_test/features/Auth/domain/repository/auth_repository.dart';
import 'package:digi_edge_test/features/Home/presentation/views/home_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class RegisterController extends GetxController {
  final AuthRepository repository = Get.find();

  final GlobalKey<FormState> registerKey = GlobalKey<FormState>();
  var registerCountryCode = CountryCode(
          country: 'United Arab Emirates', dialCode: '+971', flag: '🇦🇪')
      .obs;
  final TextEditingController nameTEC = TextEditingController();
  final TextEditingController phoneTEC = TextEditingController();
  final TextEditingController emailTEC = TextEditingController();
  final TextEditingController passwordTEC = TextEditingController();
  final TextEditingController confirmPasswordTEC = TextEditingController();
  final obsecurePassword = true.obs;
  final obsecureConfirmPassword = true.obs;

  void register() async {
    if (registerKey.currentState!.validate()) {
      Get.dialog(const LoadingDialog(), barrierDismissible: false);
      var action = await repository.register(
          name: nameTEC.text,
          countryCode: registerCountryCode.value.dialCode,
          phone: phoneTEC.text,
          email: emailTEC.text,
          password: passwordTEC.text,
          confirmPassword: confirmPasswordTEC.text);
      Get.back();
      action.fold((l) {
        Alerts.error("Error", l.message);
      }, (r) {
        Get.offNamed(HomePage.routeName);
      });
    }
  }

  void changeCountryCode(newCountryCode) {
    Get.back();
    registerCountryCode(newCountryCode);
  }
}
