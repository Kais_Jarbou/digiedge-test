import 'dart:developer';

import 'package:digi_edge_test/core/services/local_storage_service.dart';
import 'package:digi_edge_test/features/Home/presentation/views/home_page.dart';
import 'package:get/get.dart';

class WelcomeController extends GetxController {
  final LocalStorageService localStorageService = Get.find();
  final mustAuthenticate = false.obs;

  @override
  void onReady() {
    log('Welcome onReady ------');
    checkIsLoggedIn();
    super.onReady();
  }

  void checkIsLoggedIn() async {
    try {
      if (localStorageService.isLoggedIn) {
        Future.delayed(const Duration(seconds: 2), () {
          Get.offNamed(HomePage.routeName);
        });
      } else {
        Future.delayed(const Duration(seconds: 2), () {
          mustAuthenticate(true);
        });
      }
    } catch (e) {
      log('Failure', name: e.toString());
      Future.delayed(const Duration(seconds: 2), () {
        mustAuthenticate(true);
      });
    }
  }
}
