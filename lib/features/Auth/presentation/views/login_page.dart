import 'package:digi_edge_test/core/config/app_assets.dart';
import 'package:digi_edge_test/core/config/app_style.dart';
import 'package:digi_edge_test/core/reusable_widgets/app_button.dart';
import 'package:digi_edge_test/core/utils/validator.dart';
import 'package:digi_edge_test/features/Auth/presentation/controllers/login_controller.dart';
import 'package:digi_edge_test/features/Auth/presentation/views/register_page.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:get/route_manager.dart';

class LoginPage extends GetView<LoginController> {
  static const String routeName = '/login_page';
  const LoginPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Padding(
          padding: EdgeInsets.symmetric(
              vertical: 16 + MediaQuery.of(context).viewPadding.bottom,
              horizontal: 50),
          child: Form(
            key: controller.loginKey,
            child: Column(
              children: [
                Image.asset(
                  AppAssets.logo,
                  width: MediaQuery.of(context).size.width / 4,
                ),
                const SizedBox(
                  height: 16,
                ),
                const Text(
                  'Login',
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.w700),
                ),
                const Expanded(child: SizedBox()),
                TextFormField(
                    controller: controller.emailTEC,
                    textAlign: TextAlign.center,
                    validator: (value) => Validator.isNotEmpty(value),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    keyboardType: TextInputType.emailAddress,
                    decoration: AppStyle.inputDecoration.copyWith(
                      hintText: 'Email',
                    )),
                const SizedBox(
                  height: 8,
                ),
                Obx(() => TextFormField(
                    controller: controller.passwordTEC,
                    textAlign: TextAlign.center,
                    validator: (value) => Validator.isNotEmpty(value),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    keyboardType: TextInputType.emailAddress,
                    obscureText: controller.obsecurePassword.value,
                    decoration: AppStyle.inputDecoration.copyWith(
                        hintText: 'Password',
                        prefixIcon: const SizedBox(
                          width: 50,
                        ),
                        suffixIcon: GestureDetector(
                          onTap: () => controller.obsecurePassword(
                              !controller.obsecurePassword.value),
                          child: SizedBox(
                            width: 50,
                            child: Icon(controller.obsecurePassword.value
                                ? Icons.visibility_off
                                : Icons.visibility),
                          ),
                        )))),
                const SizedBox(
                  height: 8,
                ),
                AppButton(
                  onPressed: () {
                    controller.login();
                  },
                  title: 'Login',
                  color: AppStyle.purple,
                  textColor: AppStyle.white,
                ),
                const Expanded(child: SizedBox()),
                RichText(
                    text: TextSpan(
                        style: const TextStyle(
                            fontSize: 14,
                            color: AppStyle.purple,
                            fontFamily: 'AlexandriaFLF'),
                        children: [
                      const TextSpan(text: "Don't have an account? "),
                      TextSpan(
                          text: "Register",
                          style: const TextStyle(fontWeight: FontWeight.w700),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Get.offNamed(RegisterPage.routeName);
                            }),
                    ]))
              ],
            ),
          ),
        ),
      ),
    );
  }
}
