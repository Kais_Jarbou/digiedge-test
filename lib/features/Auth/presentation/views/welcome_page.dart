import 'package:digi_edge_test/core/config/app_assets.dart';
import 'package:digi_edge_test/core/config/app_style.dart';
import 'package:digi_edge_test/core/reusable_widgets/app_button.dart';
import 'package:digi_edge_test/features/Auth/presentation/controllers/welcome_controller.dart';
import 'package:digi_edge_test/features/Auth/presentation/views/login_page.dart';
import 'package:digi_edge_test/features/Auth/presentation/views/register_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class WelcomePage extends GetView<WelcomeController> {
  static const String routeName = '/welcome_page';
  const WelcomePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(),
        body: Center(
          child: Padding(
            padding: EdgeInsets.symmetric(
                vertical: 16 + MediaQuery.of(context).viewPadding.bottom,
                horizontal: 32),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.asset(
                  AppAssets.logo,
                  width: MediaQuery.of(context).size.width / 4,
                ),
                Obx(
                  () {
                    if (!controller.mustAuthenticate.value) {
                      return const SizedBox();
                    }
                    return Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          const SizedBox(
                            height: 16,
                          ),
                          const Text(
                            'Welcome to the app',
                            style: TextStyle(
                                fontSize: 24, fontWeight: FontWeight.w700),
                          ),
                          const Expanded(child: SizedBox()),
                          AppButton(
                            onPressed: () {
                              Get.offNamed(LoginPage.routeName);
                            },
                            title: 'Login',
                            color: AppStyle.purple,
                            textColor: AppStyle.white,
                          ),
                          const SizedBox(height: 8),
                          AppButton(
                            onPressed: () {
                              Get.offNamed(RegisterPage.routeName);
                            },
                            title: 'Register',
                            color: AppStyle.white,
                            textColor: AppStyle.purple,
                            borderColor: AppStyle.purple,
                          ),
                          const Expanded(child: SizedBox()),
                          RichText(
                              text: const TextSpan(
                                  style: TextStyle(
                                      fontSize: 14,
                                      color: AppStyle.purple,
                                      fontFamily: 'AlexandriaFLF'),
                                  children: [
                                TextSpan(text: "Designed & Developed by "),
                                TextSpan(
                                  text: "Ali Fouad",
                                  style: TextStyle(
                                      fontWeight: FontWeight.w700,
                                      decoration: TextDecoration.underline),
                                ),
                              ]))
                        ],
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ));
  }
}
