import 'package:digi_edge_test/core/config/app_style.dart';
import 'package:digi_edge_test/features/Auth/domain/entities/country_code.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CountryCodesDialog extends StatelessWidget {
  final CountryCode selectedValue;
  final List<CountryCode> countryCodes;
  final Function onChange;
  const CountryCodesDialog(
      {super.key,
      required this.countryCodes,
      required this.onChange,
      required this.selectedValue});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      backgroundColor: AppStyle.white,
      child: Container(
        padding: const EdgeInsets.all(24),
        height: MediaQuery.of(context).size.height / 2,
        child: Column(
          children: [
            const Text(
              'Country codes',
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
            ),
            const SizedBox(
              height: 8,
            ),
            Expanded(
              child: ListView(
                children: countryCodes
                    .map((CountryCode element) => InkWell(
                          onTap: () {
                            onChange(element);
                            Get.back();
                          },
                          child: Opacity(
                            opacity: element.dialCode == selectedValue.dialCode
                                ? 1
                                : 0.5,
                            child: SizedBox(
                              height: 24,
                              child: Row(
                                children: [
                                  Text(element.flag),
                                  Text(
                                    '(${element.dialCode}) ',
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Expanded(
                                      child: Text(
                                    element.country,
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold),
                                    maxLines: 1,
                                  )),
                                ],
                              ),
                            ),
                          ),
                        ))
                    .toList(),
              ),
            )
          ],
        ),
      ),
    );
  }
}
