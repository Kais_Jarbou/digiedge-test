import 'package:digi_edge_test/core/services/http_service.dart';
import 'package:digi_edge_test/core/utils/api_response.dart';
import 'package:get/instance_manager.dart';
import 'package:http/http.dart' as http;

class UserInfoApi {
  final String baseUrl = HttpService.baseUrl;
  final HttpService httpService = Get.find();

  Future<ApiResponse> updateInfo(
      {required String name,
      required String countryCode,
      required String phone,
      required String email}) async {
    http.Response response =
        await httpService.post(Uri.parse('$baseUrl/user/update'), body: {
      'name': name,
      'email': email,
      'country_code': countryCode,
      'phone': phone,
    });
    ApiResponse apiResponse = ApiResponse.fromJson(response);
    return apiResponse;
  }

  Future<ApiResponse> changePassword({
    required String password,
    required String confirmPassword,
    required String currentPassword,
  }) async {
    http.Response response = await httpService
        .post(Uri.parse('$baseUrl/user/changepassword'), body: {
      'password': password,
      'password_confirm': confirmPassword,
      'current_password': currentPassword,
    });
    ApiResponse apiResponse = ApiResponse.fromJson(response);
    return apiResponse;
  }

  Future<ApiResponse> deleteAccount() async {
    http.Response response = await httpService.delete(
      Uri.parse('$baseUrl/user/delete'),
    );
    ApiResponse apiResponse = ApiResponse.fromJson(response);
    return apiResponse;
  }
}
