import 'package:digi_edge_test/core/controllers/country_codes_controller.dart';
import 'package:digi_edge_test/core/reusable_widgets/confirm_dialog.dart';
import 'package:digi_edge_test/core/reusable_widgets/loading_dialog.dart';
import 'package:digi_edge_test/features/Auth/domain/entities/user.dart';
import 'package:digi_edge_test/features/Auth/presentation/views/welcome_page.dart';
import 'package:digi_edge_test/features/Home/domain/repositories/home_repository.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../../../core/utils/alerts.dart';

class HomeController extends GetxController {
  final HomeRepository homeRepository = Get.find();

  var hasData = false.obs;
  var initializedInfo = false.obs;
  late final Rx<User> user;

  @override
  void onInit() {
    getUser();
    super.onInit();
  }

  void getUser() async {
    var action = await homeRepository.getUser();
    action.fold((l) {
      Alerts.error("Error", l.message);
    }, (r) {
      user = r.obs;
      user(r);
      hasData.value = true;
    });
  }

  void logout() async {
    Get.dialog(ConfirmDialog(
        title: 'Logout',
        message: 'Are you sure you want to logout from your account?',
        onConfirm: () async {
          var action = await homeRepository.logout();
          action.fold((l) {
            Alerts.error("Error", l.message);
          }, (r) {
            Get.offAllNamed(WelcomePage.routeName);
          });
        }));
  }

  // ---------------------UPDATE INFO---------------------

  final GlobalKey<FormState> updateInfoKey = GlobalKey<FormState>();
  // ignore: prefer_typing_uninitialized_variables
  late final infoCountryCode;
  final TextEditingController nameTEC = TextEditingController();
  final TextEditingController phoneTEC = TextEditingController();
  final TextEditingController emailTEC = TextEditingController();

  Future<void> initInfo() async {
    CountryCodesController codesController = Get.find<CountryCodesController>();
    if (initializedInfo.value) {
      infoCountryCode(codesController.countryCodes
          .firstWhere((element) => element.dialCode == user.value.countryCode));
      nameTEC.text = user.value.fullName;
      emailTEC.text = user.value.email;
      phoneTEC.text = user.value.phoneNumber;
      return;
    }
    Get.dialog(const LoadingDialog(), barrierDismissible: false);
    await codesController.loadCountryCodes();

    infoCountryCode = codesController.countryCodes
        .firstWhere((element) => element.dialCode == user.value.countryCode)
        .obs;
    nameTEC.text = user.value.fullName;
    emailTEC.text = user.value.email;
    phoneTEC.text = user.value.phoneNumber;
    Get.back();
    initializedInfo(true);
    return;
  }

  void updateInfo() async {
    if (updateInfoKey.currentState!.validate()) {
      Get.dialog(const LoadingDialog(), barrierDismissible: false);
      var action = await homeRepository.updateInfo(
          email: emailTEC.text,
          name: nameTEC.text,
          phone: phoneTEC.text,
          countryCode: infoCountryCode.value.dialCode);
      Get.back();
      action.fold((l) {
        Alerts.error("Error", l.message);
      }, (r) {
        user(r);
        Get.back();
        Alerts.success('Success', 'Your information is updated successfully');
      });
    }
  }

  // ---------------------CHANGE PASSWORD---------------------

  final GlobalKey<FormState> changePasswordKey = GlobalKey<FormState>();

  final TextEditingController currentPasswordTEC = TextEditingController();
  final TextEditingController passwordTEC = TextEditingController();
  final TextEditingController confirmPasswordTEC = TextEditingController();
  final obsecureCurrentPassword = true.obs;
  final obsecurePassword = true.obs;
  final obsecureConfirmPassword = true.obs;
  void changePassword() async {
    if (changePasswordKey.currentState!.validate()) {
      Get.dialog(const LoadingDialog(), barrierDismissible: false);
      var action = await homeRepository.changePassword(
          password: passwordTEC.text,
          confirmPassword: confirmPasswordTEC.text,
          currentPassword: currentPasswordTEC.text);
      Get.back();
      action.fold((l) {
        Alerts.error("Error", l.message);
      }, (r) {
        Get.back();
        Alerts.success('Success', 'Your password is changed successfully');
      });
    }
  }

  // -------------------------DELETE ACCOUNT--------------------------

  void deleteAccount() async {
    Get.dialog(ConfirmDialog(
        title: 'Delete account',
        message:
            'Your data will be deleted and cannot be restored after that, are you sure you want to delete your account?',
        onConfirm: () async {
          Get.back();
          Get.dialog(const LoadingDialog(), barrierDismissible: false);
          var action = await homeRepository.deleteAccount();
          Get.back();
          action.fold((l) {
            Alerts.error("Error", l.message);
          }, (r) {
            Get.offAllNamed(WelcomePage.routeName);
          });
        }));
  }
}
