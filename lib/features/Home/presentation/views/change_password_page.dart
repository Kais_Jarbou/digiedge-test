import 'package:digi_edge_test/core/config/app_style.dart';
import 'package:digi_edge_test/features/Home/presentation/controllers/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/reusable_widgets/app_button.dart';
import '../../../../core/utils/validator.dart';

class ChangePasswordPage extends GetView<HomeController> {
  static const String routeName = '/change_password_page';
  const ChangePasswordPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppStyle.purple,
        leading: const BackButton(
          color: AppStyle.white,
        ),
        title: const Text(
          'Change Password',
          style: TextStyle(color: AppStyle.white, fontWeight: FontWeight.bold),
        ),
      ),
      body: Padding(
          padding: EdgeInsets.symmetric(
              vertical: 16 + MediaQuery.of(context).viewPadding.bottom,
              horizontal: 50),
          child: Form(
              key: controller.changePasswordKey,
              child: Column(children: [
                Obx(() => TextFormField(
                    controller: controller.currentPasswordTEC,
                    textAlign: TextAlign.center,
                    validator: (value) => Validator.isNotEmpty(value),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    keyboardType: TextInputType.emailAddress,
                    obscureText: controller.obsecureCurrentPassword.value,
                    decoration: AppStyle.inputDecoration.copyWith(
                        hintText: 'Current Password',
                        prefixIcon: const SizedBox(
                          width: 50,
                        ),
                        suffixIcon: GestureDetector(
                          onTap: () => controller.obsecureCurrentPassword(
                              !controller.obsecureCurrentPassword.value),
                          child: SizedBox(
                            width: 50,
                            child: Icon(controller.obsecureCurrentPassword.value
                                ? Icons.visibility_off
                                : Icons.visibility),
                          ),
                        )))),
                const SizedBox(
                  height: 8,
                ),
                Obx(() => TextFormField(
                    controller: controller.passwordTEC,
                    textAlign: TextAlign.center,
                    validator: (value) => Validator.isNotEmpty(value),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    keyboardType: TextInputType.emailAddress,
                    obscureText: controller.obsecurePassword.value,
                    decoration: AppStyle.inputDecoration.copyWith(
                        hintText: 'Password',
                        prefixIcon: const SizedBox(
                          width: 50,
                        ),
                        suffixIcon: GestureDetector(
                          onTap: () => controller.obsecurePassword(
                              !controller.obsecurePassword.value),
                          child: SizedBox(
                            width: 50,
                            child: Icon(controller.obsecurePassword.value
                                ? Icons.visibility_off
                                : Icons.visibility),
                          ),
                        )))),
                const SizedBox(
                  height: 8,
                ),
                Obx(() => TextFormField(
                    controller: controller.confirmPasswordTEC,
                    textAlign: TextAlign.center,
                    validator: (value) => Validator.isNotEmpty(value),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    keyboardType: TextInputType.emailAddress,
                    obscureText: controller.obsecureConfirmPassword.value,
                    decoration: AppStyle.inputDecoration.copyWith(
                        hintText: 'Confirm Password',
                        prefixIcon: const SizedBox(
                          width: 50,
                        ),
                        suffixIcon: GestureDetector(
                          onTap: () => controller.obsecureConfirmPassword(
                              !controller.obsecureConfirmPassword.value),
                          child: SizedBox(
                            width: 50,
                            child: Icon(controller.obsecureConfirmPassword.value
                                ? Icons.visibility_off
                                : Icons.visibility),
                          ),
                        )))),
                const SizedBox(
                  height: 8,
                ),
                AppButton(
                  onPressed: () {
                    controller.changePassword();
                  },
                  title: 'Save',
                  color: AppStyle.purple,
                  textColor: AppStyle.white,
                ),
              ]))),
    );
  }
}
