import 'package:digi_edge_test/core/config/app_style.dart';
import 'package:digi_edge_test/core/controllers/country_codes_controller.dart';
import 'package:digi_edge_test/core/reusable_widgets/app_button.dart';
import 'package:digi_edge_test/features/Home/presentation/controllers/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/utils/validator.dart';

class UserInfoPage extends GetView<HomeController> {
  static const String routeName = '/user_info_page';
  const UserInfoPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppStyle.purple,
          leading: const BackButton(
            color: AppStyle.white,
          ),
          title: const Text(
            'Update Information',
            style:
                TextStyle(color: AppStyle.white, fontWeight: FontWeight.bold),
          ),
        ),
        body: Obx(() {
          if (!controller.hasData.value) {
            return const SizedBox();
          } else {
            return Padding(
                padding: EdgeInsets.symmetric(
                    vertical: 16 + MediaQuery.of(context).viewPadding.bottom,
                    horizontal: 50),
                child: Form(
                  key: controller.updateInfoKey,
                  child: Column(
                    children: [
                      TextFormField(
                          controller: controller.nameTEC,
                          textAlign: TextAlign.center,
                          validator: (value) => Validator.isNotEmpty(value),
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          keyboardType: TextInputType.name,
                          decoration: AppStyle.inputDecoration.copyWith(
                            hintText: 'Full Name',
                          )),
                      const SizedBox(
                        height: 8,
                      ),
                      TextFormField(
                          controller: controller.phoneTEC,
                          textAlign: TextAlign.center,
                          validator: (value) => Validator.isNotEmpty(value),
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          keyboardType: TextInputType.phone,
                          decoration: AppStyle.inputDecoration.copyWith(
                              contentPadding: const EdgeInsets.only(right: 70),
                              hintText: 'Phone',
                              prefixIcon: Obx(() => GestureDetector(
                                    onTap: () =>
                                        Get.find<CountryCodesController>()
                                            .showCountryCodes(
                                                controller.infoCountryCode
                                                    .value, (value) {
                                      controller.infoCountryCode(value);
                                    }),
                                    child: Container(
                                      width: 70,
                                      alignment: Alignment.centerLeft,
                                      child: Center(
                                        child: Text(
                                            '${controller.infoCountryCode.value.flag}(${controller.infoCountryCode.value.dialCode})'),
                                      ),
                                    ),
                                  )))),
                      const SizedBox(
                        height: 8,
                      ),
                      TextFormField(
                          controller: controller.emailTEC,
                          textAlign: TextAlign.center,
                          validator: (value) => Validator.isNotEmpty(value),
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          keyboardType: TextInputType.emailAddress,
                          decoration: AppStyle.inputDecoration.copyWith(
                            hintText: 'Email',
                          )),
                      const SizedBox(
                        height: 8,
                      ),
                      AppButton(
                        onPressed: () {
                          controller.updateInfo();
                        },
                        title: 'Save',
                        color: AppStyle.purple,
                        textColor: AppStyle.white,
                      ),
                    ],
                  ),
                ));
          }
        }));
  }
}
