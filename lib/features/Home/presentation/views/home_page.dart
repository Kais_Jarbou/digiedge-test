import 'package:digi_edge_test/core/config/app_style.dart';
import 'package:digi_edge_test/features/Home/presentation/controllers/home_controller.dart';
import 'package:digi_edge_test/features/Home/presentation/views/change_password_page.dart';
import 'package:digi_edge_test/features/Home/presentation/views/user_info_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class HomePage extends GetView<HomeController> {
  static const String routeName = '/home_page';
  HomePage({super.key});

  final List<HomeOption> options = [
    HomeOption(
        title: 'Update Information',
        onTap: () async {
          await Get.find<HomeController>().initInfo();
          Get.toNamed(UserInfoPage.routeName);
        }),
    HomeOption(
        title: 'Change Password',
        onTap: () {
          Get.toNamed(ChangePasswordPage.routeName);
        }),
    HomeOption(
        title: 'Delete Account',
        onTap: () {
          Get.find<HomeController>().deleteAccount();
        }),
    HomeOption(
        title: 'Logout',
        onTap: () {
          Get.find<HomeController>().logout();
        }),
  ];

  get itemCount => null;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: AppStyle.purple,
          leading: const SizedBox(),
          title: const Text(
            'Home Page',
            style:
                TextStyle(color: AppStyle.white, fontWeight: FontWeight.bold),
          ),
        ),
        body: Obx(
          () {
            if (!controller.hasData.value) {
              return const SizedBox();
            } else {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    ListTile(
                      title: Text(controller.user.value.fullName),
                      leading: const Icon(Icons.person_2_outlined),
                    ),
                    ListTile(
                      title: Text(
                          '${controller.user.value.countryCode} ${controller.user.value.phoneNumber}'),
                      leading: const Icon(Icons.phone_android),
                    ),
                    ListTile(
                      title: Text(controller.user.value.email),
                      leading: const Icon(Icons.email_outlined),
                    ),
                    const SizedBox(
                      height: 16,
                    ),
                    ListView.separated(
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          return InkWell(
                            onTap: () => options[index].onTap(),
                            child: Container(
                                padding: const EdgeInsets.all(12),
                                decoration: BoxDecoration(
                                    color: AppStyle.white,
                                    borderRadius: BorderRadius.circular(8),
                                    boxShadow: [
                                      BoxShadow(
                                          offset: const Offset(1, 2),
                                          color:
                                              AppStyle.purple.withOpacity(0.1))
                                    ]),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      options[index].title,
                                      style: const TextStyle(
                                          fontSize: 18,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    const Icon(
                                      Icons.chevron_right,
                                      size: 30,
                                    ),
                                  ],
                                )),
                          );
                        },
                        separatorBuilder: (context, index) => const SizedBox(
                              height: 8,
                            ),
                        itemCount: options.length)
                  ],
                ),
              );
            }
          },
        ));
  }
}

class HomeOption {
  final String title;
  final Function onTap;

  HomeOption({
    required this.title,
    required this.onTap,
  });
}
