import 'package:dartz/dartz.dart';
import 'package:digi_edge_test/core/services/local_storage_service.dart';
import 'package:digi_edge_test/core/utils/api_response.dart';
import 'package:digi_edge_test/core/utils/failure.dart';
import 'package:digi_edge_test/features/Auth/data/models/user_model.dart';
import 'package:digi_edge_test/features/Auth/domain/entities/user.dart';
import 'package:digi_edge_test/features/Home/data/data_sources/user_info_api.dart';
import 'package:get/instance_manager.dart';

class HomeRepository {
  UserInfoApi api = Get.find();
  LocalStorageService localStorageService = Get.find();

  Future<Either<Failure, User>> updateInfo({
    required String name,
    required String countryCode,
    required String phone,
    required String email,
  }) async {
    try {
      ApiResponse response = await api.updateInfo(
        name: name,
        countryCode: countryCode,
        phone: phone,
        email: email,
      );
      if (response.success) {
        UserModel userModel = UserModel.fromJson(response.data);
        localStorageService.setUser(userModel);
        return Right(User.fromModel(userModel));
      } else {
        return Left(Failure(message: response.message));
      }
    } catch (e) {
      return Left(Failure(message: e.toString()));
    }
  }

  Future<Either<Failure, User>> getUser() async {
    try {
      return Right(User.fromModel(localStorageService.user));
    } catch (e) {
      return Left(Failure(message: e.toString()));
    }
  }

  Future<Either<Failure, void>> logout() async {
    try {
      return Right(await localStorageService.clearData());
    } catch (e) {
      return Left(Failure(message: e.toString()));
    }
  }

  Future<Either<Failure, void>> changePassword({
    required String password,
    required String confirmPassword,
    required String currentPassword,
  }) async {
    try {
      ApiResponse response = await api.changePassword(
          password: password,
          confirmPassword: confirmPassword,
          currentPassword: currentPassword);
      if (response.success) {
        return const Right(null);
      } else {
        return Left(Failure(message: response.message));
      }
    } catch (e) {
      return Left(Failure(message: e.toString()));
    }
  }

  Future<Either<Failure, void>> deleteAccount() async {
    try {
      ApiResponse response = await api.deleteAccount();
      if (response.success) {
        await localStorageService.clearData();
        return const Right(null);
      } else {
        return Left(Failure(message: response.message));
      }
    } catch (e) {
      return Left(Failure(message: e.toString()));
    }
  }
}
